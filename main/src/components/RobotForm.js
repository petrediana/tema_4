import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import RobotList from './RobotList'

class RobotForm extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            name : '',
            type : '',
            mass : ''
        }
        
        // handle text change to get the input values
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        }
    }
    
    render() {
        return (
            <div> 
                <input type="text" id="name" placeholder ="name" name="name" onChange={this.handleChange}/>
                <input type="text" id="type" placeholder ="type" name="type" onChange={this.handleChange}/>
                <input type="text" id="mass" placeholder ="mass" name="mass" onChange={this.handleChange}/>
        
                <input type="button" value="add" onClick={this.handleClick}/>
            </div>
            )
            
    }
}

export default RobotForm